---
layout: markdown_page
title: Leadership
---

## Guidelines

- As a leader team members will follow your behavior, always do the right thing.
- Behavior should be consistent inside and outside the company, don't fake it outside, just do the right thing inside the company as well.
- In tough times people will put it in their best efforts when they do it for eachother.
- When times are great be a voice of moderation, when times are bad be a voice of hope.
- A span of control should be around 7, from 4 to 10. Lower means too many generals and too few soldiers. Higher means you don't have time for 1:1's anymore.
- We work async, lead by example and make sure people understand that things need to be written down in issues as they happen.
- Start meetings on time, be on time yourself, don't ask if everyone is there, and don't punish people at showed up on time by waiting anyone.
- We have a simple hierarchy, everyone has one boss that is experienced in their subject matter. Matrix organizations are too hard.
- We don't have project managers. The individual contributors need to manage themselves.
- We are not a democratic or consensus driven company. People are encouraged to give their comments and opinions. But in the end one person decides the matter after have listened to all the feedback.
- It is encouraged to disagree and have a constructive confrontation, but it is "disagree & commit", when a decision is taken you make it happen.
- We give feedback, lots of it, don't hold back on suggestions to improve.
- If you praise someone try to do it in front of an audiance, if you give suggestions to improve do it 1 on 1.
- If you meet external people always ask what they think we should improve.

## Articles

- [eShares Manager’s FAQ](https://readthink.com/a-managers-faq-35858a229f84)
- [eShares How to hire](https://blog.esharesinc.com/how-to-hire-34f4ded5f176)
- [How Facebook Tries to Prevent Office Politics](https://hbr.org/2016/06/how-facebook-tries-to-prevent-office-politics)
- [The Management Myth](http://www.theatlantic.com/magazine/archive/2006/06/the-management-myth/304883/)
- [Later Stage Advice for Startups](http://themacro.com/articles/2016/07/later-stage-advice-for-startups/)
- [Mental Models I Find Repeatedly Useful](https://medium.com/@yegg/mental-models-i-find-repeatedly-useful-936f1cc405d)
- [This Is The Most Difficult Skill For CEOs To Learn](http://www.businessinsider.com/whats-the-most-difficult-ceo-skill-managing-your-own-psychology-2011-4)

## Books

- High output management - Andrew Grove ([top 10](https://getlighthouse.com/blog/andy-grove-quotes-leadership-high-output-management/))
- The Hard thing about hard things - Ben Horowitz
- [The score takes care of itself - Bill Walsh](http://coachjacksonspages.com/The%20Score%20Takes%20Care.pdf)
