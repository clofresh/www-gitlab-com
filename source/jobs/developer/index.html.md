---
layout: job_page
title: "Developer"
---

At GitLab, developers are highly independent and self-organized individual
contributors who work together as a tight team in a [remote and agile](/2015/09/14/remote-agile-at-gitlab/) way.

Most backend developers work on all aspects of GitLab, building features, fixing bugs, and generally improving the application.
Some developers [specialize](/jobs/specialist) and focus on a specific area, such as packaging, performance or GitLab CI.
Developers can specialize immediately after joining, or after some time, when they have gained familiarity with many areas of GitLab and find one they would like to focus on.

## Responsibilities

* Develop features from proposal to polished end result.
* Support and collaborate with our [service engineers](/jobs/service-engineer) in getting to the bottom of user-reported issues and come up with robust solutions.
* Engage with the core team and the open source community to collaborate on improving GitLab.
* Manage and review code contributed by the rest of the community and work with them to get it ready for production.
* Create and maintain documentation around features and configuration to save our users time.
* Take initiative in improving the software in small or large ways to address pain points in your own experience as a developer.
* Keep code easy to maintain and keep it easy for others to contribute code to GitLab.
* Qualify developers for hiring.

## Requirements

* You can reason about software, algorithms and performance from a high level
* You are passionate about open source
* You have worked on a production-level Rails application
* You know how to write your own Ruby gem using TDD techniques
* You share our [values](/handbook/#values), and work in accordance with those values.
* [A technical interview](/jobs/#technical-interview) is part of the hiring process for this position.

## Workflow

The basics of GitLab development can be found in the [developer onboarding](/handbook/developer-onboarding/#basics-of-gitlab-development) document.

The handbook details the complete [GitLab Workflow](/handbook/#gitlab-workflow).

## Senior Developers

Senior Developers are experienced developers who meet the following criteria:

1. Technical Skills
   a. Great programmers: are able to write modular, well-tested, and maintainable code
   b. Know a domain really well and radiate that knowledge
2. Leadership
   a. Begins to show architectural perspective
   b. Leads the design for medium to large projects with feedback from other engineers
3. Code quality
   a. Leaves code in substantially beter shape than before
   b. Fixes bugs/regressions quickly
   c. Monitors overall code quality/build failures
   d. Creates test plans
4. Communication
   a. Provides thorough and timely code feedback for peers
   b. Able to communicate clearly on technical topics
   c. Keeps issues up-to-date with progress
   d. Helps guide other merge requests to completion
   e. Helps with recruiting

## Staff Developers

A Senior Developer will be promoted to a Staff Developer when he/she has
demonstrated significant leadership to deliver high-impact projects. This may
involve a number of items:

1. Proposing new ideas, performing feasibility analyses, and scoping the work
2. Leading the design for large projects
3. Working across functional groups to deliver the project
4. Proactively identifying and reducing technical debt
5. Writing in-depth documentation that shares knowledge and radiates GitLab technical strengths
6. The ability to create innovative solutions that push GitLab's technical abilities ahead of the curve
7. Identifies significant projects that result in substantial cost savings or revenue
8. Proactively defines and solves important architectural issues
9. TODO

## Internships

We normally don't offer any internships, but if you get a couple of merge requests
accepted we'll interview you for one. This will be a remote internship without
supervision, you'll only get feedback on your merge requests. If you want to
work on open source and qualify please [submit an application](https://gitlab.workable.com/jobs/106660/candidates/new).
In the cover letter field please note that you want an internship and link to
the accepted merge requests. The merge requests should be of significant
value and difficulty, which is at the discretion of the hiring manager. For
example, fixing 10 typos isn't as valuable as shipping 2 new features.

## Relevant links

- [Engineering Handbook](/handbook/engineering)
- [Engineering Workflow](/handbook/engineering/workflow)
